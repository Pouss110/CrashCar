﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public GameObject button;
    private float[] choice = new float[3] {2.1f,0,-2.1f};

    
    public GUIText gameOverText;
    private bool gameOver;
    // Use this for initialization
    void Start () {
        StartCoroutine(spawnWaves());
        gameOver = false;
        gameOverText.text = "";
        button = GameObject.FindGameObjectWithTag("RestartButton");
        button.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator spawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                
                Vector3 spawnPosition = new Vector3(choice[Random.Range(0,choice.Length)], spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;

                GameObject instance = Instantiate(hazard, spawnPosition, spawnRotation) as GameObject;
                GameObject core = instance.transform.Find("core").gameObject;
                Renderer mat = core.GetComponent<Renderer>();
                mat.material.color = Random.ColorHSV();

                yield return new WaitForSeconds(spawnWait);
                if (gameOver)
                {
                    //restartText.text = "Press 'R' for Restart";
                    //restart = true;
                    break;
                }
            }
            yield return new WaitForSeconds(waveWait);
            if (gameOver)
            {
               
                button.SetActive(true);
                //restartText.text = "Press 'R' for Restart";
                //restart = true;
                break;
            }
        }
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over";
        gameOver = true;
    }

    public void Restart()
    {
        SceneManager.LoadScene("main_scene");
    }
}
