﻿using UnityEngine;
using System.Collections;

public class Contact : MonoBehaviour {
    public GameObject explosion;
   // public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

        if (gameController == null)
        {
            Debug.Log("Cannot find game controller");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Boundary")
        {
            Instantiate(explosion, transform.position, Quaternion.Euler(-90,0,0));
            if (other.tag == "Player")
            {
                
                Instantiate(explosion, other.transform.position, other.transform.rotation);
                gameController.GameOver();
            }
           

            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
