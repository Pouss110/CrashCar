﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    private Rigidbody rg;
    private float adaptX, adaptZ;
    public float speed ;
    public GUIText text;
    private float distance;
    public Boundary boundary;
    [System.Serializable]
    public class Boundary
    {
        public float xMin, xMax, zMin, zMax;
    }
    // Use this for initialization
    void Start () {
        rg = GetComponent<Rigidbody>();
        text.text = "dfdfd";
	}

    // Update is called once per frame
   
    void Update()
    {
        
       

    }

    void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            Vector3 touchPosition = Input.GetTouch(0).position;
            Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, -13.0f));
            text.text = "touché " + position.x + "  " + position.y + " " + position.z;
            if (position.x > 0)
            {
                rg.velocity = new Vector3(-5 * speed, 0, 0);
                text.text = "ici";

            }
            else
            {
                rg.velocity = new Vector3(5 * speed, 0, 0);
                text.text = "la";
            }
        }
        else
        {
            rg.velocity = new Vector3(0, 0, 0);
        }
        rg.position = new Vector3
           (
               Mathf.Clamp(rg.position.x, boundary.xMin, boundary.xMax),
               0.0f,
               Mathf.Clamp(rg.position.z, boundary.zMin, boundary.zMax)
           );
    }
}
